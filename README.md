# Lxc Deployment

Simple script to create a given amount of LXC containers including your SSH public key.

```shell
ufw allow in on lxcbr0
ufw allow out on lxcbr0
```
