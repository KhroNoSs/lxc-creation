#!/bin/bash

distros=( debian ubuntu fedora archlinux almalinux alpine busybox centos devuan kali mint opensuse oracle rockylinux voidlinux)

usage="Usage : $0 [ -n name of the containers] [ -c containers count] [ -t container template] [-r release]"

# Default Values

name="lxc_generator-${RANDOM}"
count="1"
template="debian"
release="bullseye"

# Arguments

while getopts ":hn:c:t:r:" option; do
   case $option in
      h) # display Help
         Help
         exit;;
      n)
         name=$OPTARG;;
      c)
         count=$OPTARG;;
      t)
         template=$OPTARG;;
      r)
         release=$OPTARG;;
     \?)
		 echo "${usage}"
         exit;;
   esac
done

# Template name check

if [[ ! "${distros[*]}" =~ "${template}" ]] ; then
	echo "${template} is not a valid distrib name" ; exit 1
fi

# If multiple SSH keys exist in ~/.ssh, ask which one should be used

if [ $(ls -1q $HOME/.ssh/*.pub | wc -l) -gt 1 ] ; then
	keys=$(ls -1q "$HOME"/.ssh/*.pub) 
	echo "$(echo "${keys}" | nl)"
	read -rp "Which key would you want to use ? [int]"
	sshKey=$(sed -n -e "${REPLY}p" $keys)
else
	sshKey=$(find $HOME/.ssh/ -type f -name "*.pub" -exec cat {} \; -quit)
fi

# Create the first container

sudo -s -- << EOF
	lxc-create -t download --name ${name}1 -- -d ${template} -r ${release} -a amd64
	lxc-start ${name}1
	sleep 5
	if [ ${template} = "debian" ] || [ ${template} = "ubuntu" ] || [ ${template} = "mint" ] || [ ${template} = "devuan" ] || [ ${template} = "kali" ] ; then
		lxc-attach ${name}1 -- sh -c "apt-get update && apt-get install -y python3 openssh-server"
	elif [ ${template} = "fedora" ] || [ ${template} = "almalinux" ] || [ ${template} = "centos" ] || [ ${template} = "oracle" ] || [ ${template} = "rockylinux" ] ; then
		lxc-attach ${name}1 -- sh -c "dnf install -y python3 openssh-server"
	elif [ ${template} = "archlinux" ] ; then
		lxc-attach ${name}1 -- sh -c "pacman --noconfirm python3 openssh-server" 
	elif [ ${template} = "voidlinux" ] ; then
		lxc-attach ${name}1 -- sh -c "xbps-install -y python3 openssh-server" 
	elif [ ${template} = "alpine" ] ; then
		lxc-attach ${name}1 -- sh -c "apk add python3 openssh-server"
	elif [ ${template} = "opensuse" ] ; then
		lxc-attach ${name}1 -- sh -c "zypper -n install python3 openssh-server"
	fi
	mkdir /var/lib/lxc/${name}1/rootfs/root/.ssh/
	sudo lxc-stop ${name}1
	bash -c "echo $sshKey > /var/lib/lxc/${name}1/rootfs/root/.ssh/authorized_keys"
EOF

# Clone the first container x times to match $count

for x in $(seq 2 "${count}") ; do
sudo -s -- << EOF
	lxc-copy -n ${name}1 -N ${name}${x}
	lxc-start ${name}${x}
EOF
done

# Start the first container (needed to be stopped for cloning to work)

sudo lxc-start "${name}"1
sleep 12

for n in $(sudo lxc-ls) ; do 
	sudo lxc-info "$n"; 
done | grep 'Name:\|IP:' | awk '{print $2}' | paste -d " " - - | awk '{print $2,$1}' | sudo tee -a /etc/hosts
